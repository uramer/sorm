#include <vector>
#include <string>
#include <string_view>
#include <typeindex>
#include <variant>

#include "minimal-hash.hpp"

#ifndef SORM_MAIN
#define SORM_MAIN

namespace sorm {
  using Value = std::variant<std::string>;
  template <typename Data>
  using Getter = Value(*)(const Data&);
  template <typename Data>
  using Setter = void(*)(const Data&, Value v);

  template<typename Data>
  struct Field {
    std::string mName;
    Getter<Data> mGetter;

    constexpr Field(std::string_view name, Getter<Data> getter)
      : mName(name)
      , mGetter(getter)
    {}
  };

  template<typename Data, size_t N>
  class Fields {
    private:
      using Field = Field<Data>;
      std::array<Field, N> mFields;
      PerfectHashMap<N> mNameMap;

      static constexpr std::array<std::string_view, N> unpackNames(const std::array<Field, N>& fields) {
        std::array<std::string_view, N> names;
        for (size_t i = 0; i < N; ++i)
          names[i] = fields[i].mName;
        return names;
      }
    public:
      constexpr Fields(std::array<Field, N> fields)
        : mFields(fields)
        , mNameMap(unpackNames(mFields))
      {}

      constexpr const Field& at(size_t index) const {
        return mFields[index];
      }

      constexpr const Field& at(std::string_view name) const {
        return mFields[mNameMap.at(name)];
      }

      constexpr size_t indexOf(std::string_view name) const {
        return mNameMap.at(name);
      }

      constexpr size_t size() const {
        return N;
      }
  };
}

#endif SORM_MAIN
