#include <cassert>
#include <string_view>
#include <initializer_list>
// replace with some static_vector implementation?
#include <vector>
#include <array>
#include <optional>
#include <algorithm>

#ifndef SORM_MINIMAL_HASH
#define SORM_MINIMAL_HASH

using hash_t = uint64_t;

constexpr hash_t xorshift(hash_t state) {
    hash_t x = state;
    x ^= x << 13;
    x ^= x >> 7;
    x ^= x << 17;
    return state = x;
}

constexpr hash_t power(hash_t base, hash_t exponent) {
    hash_t result = 1;
    for (;;) {
        result *= (exponent & 1) * base + (1 - exponent & 1);
        exponent >>= 1;
        if (exponent == 0)
            break;
        base *= base;
    }
    return result;
}

constexpr hash_t fitToRange(hash_t min, hash_t max, hash_t value) {
    // TODO: check if this matters for likelyPrime's performance
    if (max <= min) return min;
    return (min + value) % (max - min);
}

constexpr hash_t factor2(hash_t n) {
    hash_t result = 0;
    while (n % 2 == 0) {
        n >>= 1;
        ++result;
    }
    return result;
}

// Miller–Rabin test
constexpr bool isLikelyPrime(hash_t n, char iterations) {
    hash_t s = factor2(n - 1);
    if (s == 0) return false;
    hash_t d = (n - 1) / (static_cast<hash_t>(1) << s);
    hash_t xorshiftState = 7919;
    for (char i = 0; i < iterations; ++i) {
        xorshiftState = xorshift(xorshiftState);
        hash_t base = fitToRange(2, n - 2, xorshiftState);
        hash_t x = power(base, d) % n;
        hash_t y = 0; // 
        for (hash_t j = 0; j < s; ++j) {
            y = (x * x) % n;
            if (y == 1 && x != 1 && x != (n - 1)) {
                // nontrivial square root of 1 modulo n
                return false;
            }
            x = y;
        }
        if (y != 1)
            return false;
    }
    return true;
}

constexpr hash_t largerLikelyPrime(hash_t n, char iterations) {
    for (hash_t i = n + 2 - (1 - n % 2);; i += 2) {
        if (isLikelyPrime(i, iterations))
            return i;
    }
}

constexpr hash_t hash(std::string_view str, hash_t d = 0x811C9DC5) {
    for (char c : str)
        d = (d ^ c) * 16777619;
    return d;
}

template <size_t N>
class PerfectHashMap {
private:
    static constexpr size_t sValueCount = largerLikelyPrime(N + N / 4, 1);
    static constexpr size_t sBucketCount = std::max(static_cast<size_t>(1), sValueCount / 5);
    std::array<hash_t, sBucketCount> mG;
    std::array<size_t, sValueCount> mValues;

    constexpr inline hash_t bucketIndex(std::string_view s) const {
        return hash(s, 0) % sBucketCount;
    }
public:
    constexpr PerfectHashMap(std::array<std::string_view, N> keys)
        : mValues()
        , mG()
    {
        assert(keys.size() == N);

        std::array<std::vector<std::string_view>, sBucketCount> buckets;
        std::array<std::optional<size_t>, sValueCount> values;
        for (std::string_view key : keys)
            buckets[bucketIndex(key)].push_back(key);

        std::sort(buckets.begin(), buckets.end(), [](const auto& a, const auto& b) {
            return b.size() < a.size();
            });

        for (auto& bucket : buckets) {
            if (bucket.empty()) break;
            hash_t d;
            std::vector<hash_t> bucketMap;
            bucketMap.reserve(bucket.size());
            for (d = 1;;++d) {
                bool valid = true;
                for (size_t i = 0; i < bucket.size(); ++i) {
                    hash_t slot = hash(bucket[i], d) % sValueCount;
                    valid = !values[slot].has_value()
                        && std::find(bucketMap.begin(), bucketMap.end(), slot) == bucketMap.end();
                    if (valid) bucketMap.push_back(slot);
                    else break;
                }
                if (valid) break;
                else bucketMap.clear();
            }

            mG[bucketIndex(bucket[0])] = d;
            for (size_t i = 0; i < bucket.size(); ++i)
                values[bucketMap[i]] = std::find(keys.begin(), keys.end(), bucket[i]) - keys.begin();
        }

        for (size_t i = 0; i < values.size(); ++i) {
            mValues[i] = values[i].value_or(0);
        }
    }

    constexpr size_t at(const std::string_view& key) const {
        hash_t index = bucketIndex(key);
        hash_t d = mG[index];
        return mValues[hash(key, d) % mValues.size()];
    }
};

#endif
