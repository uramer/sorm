#include "../include/serializer-orm.hpp"

using namespace sorm;

template <typename Serializer>
class TestWriter {
  private:
    using Data = Serializer::Data;
    void write(const Data& data, size_t index) {
        sorm::Value v = Serializer::sFields.at(index).mGetter(data);
        std::visit([](const auto& value) -> void { std::cout << value; }, v);
    }
  public:
    void write(const Data& data) {
        write(data, 0);
        for (size_t i = 1; i < Serializer::sFields.size(); ++i) {
            std::cout << ", ";
            write(data, i);
        }
    }
};