project(test)
add_executable(test
  test-writer.hpp
  main.cpp
)
target_sources(test PUBLIC
  ../include/minimal-hash.hpp
  ../include/serializer-orm.hpp
)
