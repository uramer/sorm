#include <iostream>
#include <array>

#include "../include/serializer-orm.hpp"

#include "test-writer.hpp"

struct TestData {
    std::string mId;
    std::string mName;
    std::string mDescription;
};

struct TestSerializer {
    using Data = TestData;
    static sorm::Value getId(const TestData& data) {
        return data.mId;
    }
    static sorm::Value getName(const TestData& data) {
        return data.mName;
    }
    static sorm::Value getDescription(const TestData& data) {
        return data.mDescription;
    }

    static constexpr sorm::Fields<Data, 3> sFields = std::array<sorm::Field<Data>, 3>({
        sorm::Field("id", &getId),
        sorm::Field("name", &getName),
        sorm::Field("description", &getDescription),
    });
};

void test_serializer() {
    TestWriter<TestSerializer> writer;
    writer.write({
        "my id",
        "my name",
        "my description",
    });
}

int main() {
    test_serializer();
}