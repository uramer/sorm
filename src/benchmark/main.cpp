#define PICOBENCH_IMPLEMENT
#include "picobench.hpp"

int main() {
    picobench::runner runner;
    return runner.run();
}