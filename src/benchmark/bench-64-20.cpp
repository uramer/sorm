#include <iostream>
#include <unordered_map>
#include <map>
#include <algorithm>

#include "../include/minimal-hash.hpp"

#include "picobench.hpp"

PICOBENCH_SUITE("64 20-long keys");

namespace {
constexpr std::array<std::string_view, 64> keys({
"g6a1yzuVLsYS8XhVKwqb",
"E8Mkmj1xb2LJkijMuybW",
"s1YjswWsTZ6VZdmjXxZl",
"kUOIODUrcDWLmoZos3L2",
"xxHKckeMdm24UJNM8ZTr",
"uU7DnDWo1bKTRVevOwQI",
"NG6s7lFprbHCGzfLXnoB",
"qsOFv6arIARiOd4HwqBJ",
"7a04YqcocaPseb4AbmOG",
"sKkLF9aR1fDHhN7UgmZ1",
"m5efMeU3XtPWYX7F7eOj",
"05fifbHcWK02NUNTc5Kc",
"15IzLTGCkf6wUAcgWoqM",
"j10TRpVyINxCmIDVfNiP",
"M2IBD1oPMmezVnXbrx1u",
"0FQpqDLH61nuZU2PuuYc",
"ojOK717mDYZmEDychTPP",
"ws70MBRXy7RXAGcmrv5x",
"8TRl1FR45BlXgKyVRy38",
"YM4MPMygkQNP5jJQvkYi",
"EEOo3G5D1jOPWO6icxh8",
"5nCq6GR6ZVE4tRFDrCZL",
"ERlgH3hfQy4x56w77kJT",
"miYXWVXwa3s072EQIaxV",
"APmaMr4KDTWbw33Ajeuq",
"yHB8gQ16HhazJHeUSfwj",
"t4F1qmuu7msJlPXIUVUV",
"zHA3A4qy3jXfhzk05JJR",
"8ZXj441o01WdlnntrLgL",
"qimGvsEZGeJ7bfLN4HUU",
"tV7LJz1W6UONofeVkNrO",
"Km8upNYGyNjFhQJSkhgC",
"jy3t83jth5tBFD1Cl7dj",
"QiXmQL84xa3ciEEJQBFS",
"Du5Rtqwu6eE83hjXUPzx",
"ElQapGtSUi1BjeMBn1aM",
"pBlmHbSBbK49gtAILmJ0",
"QDWN3ymjqpd3fnUgYOBz",
"MT1OOzx4v5EzFWxv9vO5",
"2RPNIbYIxF3tywJnPQIG",
"HGNuPSLhqXxP3hATEKu0",
"SDDYfbs5GPjWpC9rvtKH",
"hZREJgPnAaKtFgL6224V",
"wqHlr4zqexVZB2MGoGoR",
"2P9BPqNl8RaQNS94kNX9",
"75XCJNRvx7F3MhtBR8PO",
"L2YjJHFTrwRQDs2yZXyb",
"ftv3SQWbIDsNNsSq4k8I",
"beey4at8vOvwxqw50z7s",
"vzbqmDKQB7EDonYG4zUk",
"EIV2j5OkWzDlgRi34MZx",
"Uq0NosHuGl247UFeUTvK",
"YgPayFKRzSRRLfwScetb",
"pEcEXvleFc1GA0NDpnBj",
"LBR1HwoHFSmeaf0dkati",
"tEfg6on6nESTUanhnPBP",
"TZAlXrz9SIO9mzaa6Jnh",
"pfkGRQXcQefsf7ENpEPf",
"pahp5yBp7jZREbcQJ00S",
"UWCqNJufWlZNCWU4F7la",
"pecV7XPhLltdCpdhERaH",
"W5aXd94wqy52hBwO0AOu",
"dD0OHml3GxPpJYNs2yJF",
"GNyvaVbyunpvQC3d1vwq", });

void perfect_hash(picobench::state& s) {
    constexpr PerfectHashMap map(keys);
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i) {
            size_t index = map.at(keys[i]);
            assert(index == i);
            sum += index;
        }
        s.set_result(sum);
    }
}

PICOBENCH(perfect_hash);

void std_map_bracket(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({keys[i], i});
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_map_bracket);

void std_map_at(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_map_at);

void std_unordered_map_bracket(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_bracket);

void std_unordered_map_at(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_at);


void std_find(picobench::state& s) {
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += std::find(keys.begin(), keys.end(), keys[i]) - keys.begin();
        s.set_result(sum);
    }
}
PICOBENCH(std_find);
}