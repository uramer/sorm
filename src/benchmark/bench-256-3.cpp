#include <iostream>
#include <unordered_map>
#include <map>

#include "../include/minimal-hash.hpp"

#include "picobench.hpp"

PICOBENCH_SUITE("256 simple keys");

namespace {
constexpr std::array<std::string_view, 256> keys({
"397",
"376",
"772",
"746",
"212",
"690",
"329",
"010",
"258",
"911",
"845",
"621",
"138",
"351",
"789",
"983",
"945",
"769",
"389",
"167",
"870",
"465",
"390",
"034",
"602",
"156",
"965",
"672",
"874",
"346",
"653",
"711",
"791",
"542",
"322",
"730",
"661",
"999",
"589",
"644",
"828",
"478",
"204",
"861",
"842",
"588",
"971",
"456",
"760",
"355",
"611",
"829",
"817",
"435",
"364",
"725",
"179",
"448",
"092",
"246",
"575",
"706",
"835",
"171",
"438",
"532",
"082",
"964",
"146",
"221",
"457",
"131",
"039",
"118",
"533",
"967",
"995",
"095",
"431",
"470",
"455",
"656",
"282",
"228",
"003",
"879",
"818",
"954",
"622",
"597",
"666",
"681",
"598",
"756",
"248",
"324",
"848",
"188",
"382",
"751",
"055",
"766",
"186",
"136",
"062",
"916",
"778",
"514",
"404",
"640",
"318",
"183",
"774",
"620",
"973",
"270",
"253",
"100",
"843",
"098",
"508",
"986",
"684",
"979",
"111",
"027",
"170",
"132",
"717",
"295",
"001",
"616",
"657",
"430",
"515",
"831",
"590",
"917",
"719",
"236",
"018",
"405",
"823",
"636",
"335",
"941",
"594",
"023",
"700",
"239",
"195",
"249",
"366",
"805",
"946",
"181",
"493",
"235",
"126",
"504",
"124",
"970",
"231",
"468",
"985",
"396",
"868",
"623",
"189",
"888",
"149",
"460",
"499",
"344",
"537",
"659",
"137",
"400",
"723",
"015",
"073",
"752",
"940",
"799",
"931",
"085",
"210",
"768",
"753",
"997",
"362",
"736",
"292",
"494",
"854",
"449",
"975",
"356",
"240",
"800",
"045",
"247",
"349",
"558",
"020",
"900",
"886",
"108",
"570",
"884",
"643",
"267",
"781",
"352",
"662",
"492",
"487",
"141",
"106",
"990",
"574",
"305",
"748",
"815",
"464",
"716",
"263",
"429",
"089",
"312",
"283",
"454",
"925",
"459",
"469",
"557",
"738",
"227",
"977",
"720",
"077",
"497",
"084",
"837",
"856",
"368",
"759",
"155",
"929",
"285",
"069",
"568",
"161",
"627",
"922",
"273", });

void perfect_hash(picobench::state& s) {
    constexpr PerfectHashMap map(keys);
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i) {
            size_t index = map.at(keys[i]);
            assert(index == i);
            sum += index;
        }
        s.set_result(sum);
    }
}

PICOBENCH(perfect_hash);

void std_map_bracket(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({keys[i], i});
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_map_bracket);

void std_map_at(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_map_at);

void std_unordered_map_bracket(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_bracket);

void std_unordered_map_at(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_at);


void std_find(picobench::state& s) {
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += std::find(keys.begin(), keys.end(), keys[i]) - keys.begin();
        s.set_result(sum);
    }
}
PICOBENCH(std_find);
}