#include <iostream>
#include <unordered_map>
#include <map>
#include <algorithm>
#include <string_view>

#include "../include/minimal-hash.hpp"

#include "picobench.hpp"

PICOBENCH_SUITE("32 10-long keys");

namespace {
constexpr std::array<std::string_view, 32> keys({
"gCGEqKHgba",
"jFuHBumOLc",
"u5ihpGrCd9",
"CnqwqMQy0p",
"mzoG7a21m4",
"YCzZF7SmiA",
"PmHbbKH0Ed",
"wicWmbmlhk",
"1ColEtCxPo",
"4Y5ZzDbuRn",
"11xUYq9p9Z",
"jEsDBfWpXn",
"A9o1jAbtiT",
"lESCLKL7Jl",
"shDaS7e9Qz",
"3zBJ1SsUc5",
"Bkwv8o3Mdv",
"nipouLwMxk",
"ly1Wc35nfr",
"mFQpXggzaA",
"6SetFrdVb5",
"KZAcUGOH1D",
"FaYXMLz1mJ",
"LGwEK9aJ2s",
"sGKaCJw0bD",
"VLrtLm9kCc",
"cEObM0sijZ",
"N679Avd1sU",
"bQOxeIt02O",
"E6STokRHWb",
"hFfGK2gxRQ",
"kgVbo5kZ1T", });

void perfect_hash(picobench::state& s) {
    constexpr PerfectHashMap map(keys);
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i) {
            size_t index = map.at(keys[i]);
            assert(index == i);
            sum += index;
        }
        s.set_result(sum);
    }
}

PICOBENCH(perfect_hash);

void std_map_bracket(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({keys[i], i});
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_map_bracket);

void std_map_at(picobench::state& s) {
    std::map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_map_at);

void std_unordered_map_bracket(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map[keys[i]];
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_bracket);

void std_unordered_map_at(picobench::state& s) {
    std::unordered_map<std::string_view, size_t> map;
    for (size_t i = 0; i < keys.size(); ++i)
        map.insert({ keys[i], i });
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += map.at(keys[i]);
        s.set_result(sum);
    }
}

PICOBENCH(std_unordered_map_at);


void std_find(picobench::state& s) {
    for (auto _ : s)
    {
        size_t sum = 0;
        for (size_t i = 0; i < keys.size(); ++i)
            sum += std::find(keys.begin(), keys.end(), keys[i]) - keys.begin();
        s.set_result(sum);
    }
}
PICOBENCH(std_find);
}